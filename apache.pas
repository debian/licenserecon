// Reconcile DEP-5 debian/copyright to licensecheck
//
// Copyright : 2024 P Blackman
// License   : BSD-2-clause
//
// Test for false positive on licences;  Apache-2.0 v Apache
//

unit apache;
{$mode delphi}

interface

function CheckApache (Fname : AnsiString; Dep5, Actual : String) : Boolean;


implementation uses StrUtils, support;

const  A2 : string = 'Apache-2.0';


function GetFileLicense (Fname : AnsiString) : String;
const MaxLines : Integer = 100;
var Lines : Integer;
    Line  : AnsiString;
    Lfile : Text;

begin
    result := '';
    Lines := 0;
    if OpenFile (FName, Lfile) then
    begin
        while not EOF (Lfile) and (Lines < MaxLines) do
        begin
            ReadLn (Lfile, Line);

            if IsWordPresent ('''' + A2 + '''', Line, WhiteSpace) then
            begin
                result := A2;
                Lines := MaxLines; // terminate loop
            end;
        end;
    end;
end;


// Return true if Actual is a match for d/copyright
function CheckApache (Fname : AnsiString; Dep5, Actual : String) : Boolean;
begin
    result := False;

    if (Dep5 = Actual) then
        result := True
    else
    if (Dep5 = A2) and (Actual = 'Apache') then
        if GetFileLicense (FName) = A2 then
            result := True;
end;

end.
