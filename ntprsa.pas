// Reconcile DEP-5 debian/copyright to licensecheck
//
// Copyright : 2024 P Blackman
// License   : BSD-2-clause
//
// Test for false positive when RSA license detected as NTP

unit ntprsa;
{$mode delphi}

interface

function CheckNTPRSA (Dep5, Actual: String) : Boolean;


implementation uses StrUtils;


// Return true if Actual is NTP and Dep5 is RSA-MD?
// Known false positive in licensecheck
function CheckNTPRSA (Dep5, Actual: String) : Boolean;
begin
    result := (Actual = 'NTP') and ContainsText (Dep5, 'RSA-MD')
end;

end.
