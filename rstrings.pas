unit rstrings;
{$mode delphi}

interface

resourcestring

    // lrc.pas
    rsPST = 'Parsing Source Tree';
    rsFPS = 'Failed to parse Source Tree';
    rsRl  = 'Running';
    rsFtr = 'Failed to run';
    rshul = 'has unused license';
    rsNdf = 'No significant differences found';
    rsCff = 'Cannot find the file';

    //dep5.pas
    rsSfp = 'Superfluous file pattern';
    rsRc  = 'Reading';
    rsMFP = 'Missing Files: Paragraph for';
    rsID5 = 'Invalid DEP-5 header in';

    //options.pas
    rsIuo = 'Ignoring unknown option';

implementation
end.
