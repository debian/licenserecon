// Reconcile DEP-5 debian/copyright to licensecheck
//
// Copyright : 2024 P Blackman
// License   : BSD-2-clause
//
// Test for false positive on licences;  GFDL-NIV-1.3+  vs GFDL-1.3+
//

unit gfdl;
{$mode delphi}

interface

function CheckGFDL (Fname : AnsiString; Dep5, Actual : String) : Boolean;

implementation uses StrUtils, support;


function NoInvariant (Fname : AnsiString) : Boolean;
const MaxLines : Integer = 50;
var Lines : Integer;
    Line  : AnsiString;
    Lfile : Text;

begin
    result := False;
    Lines := 0;

    if OpenFile (FName, Lfile) then
    begin
        while not EOF (Lfile) and (Lines < MaxLines) do
        begin
            ReadLn (Lfile, Line);

            if IsWordPresent ('Invariant', Line, WhiteSpace) then
            begin
                result := True;
                Lines := MaxLines; // terminate loop
            end;
        end;
    end;
end;


// Return true if Actual is a match for d/copyright
function CheckGFDL (Fname : AnsiString; Dep5, Actual : String) : Boolean;
const GN3 : String = 'GFDL-NIV-1.3+';

begin
    result := False;

    if (Dep5 = Actual) then
        result := True
    else
    if (Dep5 = GN3 ) and (Actual = 'GFDL-1.3+') then
        if NoInvariant (FName) then
            result := True;
end;

end.
