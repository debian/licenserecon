// Reconcile DEP-5 debian/copyright to licensecheck
//
// Copyright : 2025 P Blackman
// License   : BSD-2-clause
//
// Test for false positive on PSF-2 licence
//

unit psf;
{$mode delphi}

interface

function CheckPSF2 (Fname : AnsiString; Dep5, Actual : String) : Boolean;


implementation uses StrUtils, support;

const  P2 : string = 'PSF-2';
    P2txt : string = 'Python Software Foundation License Version 2';

function GetFileLicense (Fname : AnsiString) : String;
const MaxLines : Integer = 120;
var Lines : Integer;
    Line  : AnsiString;
    Lfile : Text;

begin
    result := '';
    Lines := 0;
    if OpenFile (FName, Lfile) then
    begin
        while not EOF (Lfile) and (Lines < MaxLines) do
        begin
            ReadLn (Lfile, Line);

            if (NPos (p2txt, Line, 1) > 0) then
            begin
                result := P2;
                Lines := MaxLines; // terminate loop
            end;
        end;
    end;
end;


// Return true if Actual is a match for d/copyright
function CheckPSF2 (Fname : AnsiString; Dep5, Actual : String) : Boolean;
begin
    result := False;

    if (Dep5 = Actual) then
        result := True
    else
    if (Dep5 = P2) and (Actual <> P2) then
        if GetFileLicense (FName) = P2 then
            result := True;
end;

end.
