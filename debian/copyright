Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: licenserecon
Upstream-Contact: Peter Blackman <peter@pblackman.plus.com>
Source: https://salsa.debian.org/debian/licenserecon

Files:     *
Copyright: 2023-2025 Peter Blackman <peter@pblackman.plus.com>
License:   BSD-2-clause

Files:     debian/*
Copyright: 2023-2025 Peter Blackman <peter@pblackman.plus.com>
License:   BSD-2-clause

Files:     debian/*.yml
Copyright: 2023 Simon Josefsson <simon@josefsson.org>
           2024 Peter Blackman <peter@pblackman.plus.com>
License:   BSD-2-clause

Files:     debian/tests/data/Apache.file
Copyright: 2024 P Blackman  <peter@pblackman.plus.com>
License:   Apache-2
 For testing only. Consider file as BSD-2-clause

Files:     debian/*/Apache2.file
Copyright: 2024 P Blackman  <peter@pblackman.plus.com>
License:   Apache-2.0
 For testing only. Consider file as BSD-2-clause

Files:     debian/tests/data/GFDL-NIV.file
Copyright: 2024 P Blackman  <peter@pblackman.plus.com>
License:   GFDL-NIV-1.3+
 For testing only. Consider file as BSD-2-clause

Files:     debian/tests/data/GPL3-plus.file
           debian/tests/data/SPDX.file
Copyright: 2024 P Blackman  <peter@pblackman.plus.com>
License:   GPL-3+
 For testing only. Consider files as BSD-2-clause

Files:     debian/tests/data/GPL2-plus.file
           debian/tests/data/SPDX2.file
           debian/tests/data/Eclipse1.file
Copyright: 2024 P Blackman  <peter@pblackman.plus.com>
License:   GPL-2+
 For testing only. Consider files as BSD-2-clause

Files:     debian/tests/data/LGPL2-plus.file
Copyright: 2024 P Blackman  <peter@pblackman.plus.com>
License:   LGPL-2.1+
 For testing only. Consider file as BSD-2-clause

Files:     debian/tests/data/RSA.file
Copyright: 2024 P Blackman  <peter@pblackman.plus.com>
License:   RSA-MD5
 For testing only. Consider file as BSD-2-clause

Files:     debian/tests/data/Eclipse2.file
Copyright: 2024 P Blackman  <peter@pblackman.plus.com>
License:   EPL-1.0
 For testing only. Consider file as BSD-2-clause

Files:     debian/tests/data/andor.file
Copyright: 2025 P Blackman  <peter@pblackman.plus.com>
License:   GPL-2+ or MPL-1.1
 For testing only. Consider file as BSD-2-clause

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
