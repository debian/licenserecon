// Reconcile DEP-5 debian/copyright to licensecheck
//
// Copyright : 2024 P Blackman
// License   : BSD-2-clause
//
// Test for false positive on GNU Lesser General Public License
// with 'any later version' not shown by licensecheck as GPL?+ etc

unit gpla;
{$mode delphi}

interface

function CheckGPLa (Fname : AnsiString; Dep5, Actual: String) : Boolean;


implementation uses Math, StrUtils, Support;

// test for GPL+ style license
function LaterLicense (Fname : AnsiString) : Boolean;
const Max1 : Integer = 40; // Arbitrary, license usually at start of file
      Max2 : Integer = 6;  // Arbitrary, this text usually two lines after G..P..L
var Lines,
    Lpos : Integer;
    Line : AnsiString;
    Lfile : Text;

begin
    result := false;
    Lines := 0;
    if OpenFile (FName, Lfile) then
    begin
        while not EOF (Lfile) and (Lines < Max1) do
        begin
            readln (Lfile, Line);
            Lpos := NPos ('General Public License', Line, 1);
            inc (Lines);

            if Lpos > 0 then
            begin
                Lines := 0;
                while not EOF (Lfile) and (Lines < Max2) do
                begin
                    readln (Lfile, Line);
                    Lpos := NPos ('any later version', Line, 1);
                    inc (Lines);

                    if Lpos > 0 then
                    begin
                        result := true;
                        Lines := max (Max1,Max2); // terminate loop
                    end
                end;
            end;
        end;
        Close (Lfile);
    end;
end;

// Return true if Actual should be *+ as shown in d/copyright
function CheckGPLa (Fname : AnsiString; Dep5, Actual: String) : Boolean;
begin
    if  ContainsStr (Dep5, 'GPL')
    and ContainsStr (Dep5, Actual)
    and (length (Dep5) - length (Actual) = 1)
    and (Dep5 [length(Dep5)] = '+')
    and ((FName = 'LICENSE') or LaterLicense (FName)) then
        result := true
    else
        result := false;
end;

end.
