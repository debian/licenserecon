# Makefile for licenserecon
# Copyright : 2023-2024 P Blackman
# License   : BSD-2-clause

# Add -gl -dDEBUG to CFLAGS to debug

# code won't compile in PIC mode on m68k 
ifeq ($(DEB_HOST_ARCH),m68k)
  CFLAGS = -Sj- -Co -Ci -Cr -CR -Sa -Sh- -vhqw -O4 -XX
else
  CFLAGS = -Cg -Sj- -Co -Ci -Cr -CR -Sa -Sh- -vhqw -O4 -XX
endif

# -pie not working om ppc64el
ifeq ($(DEB_HOST_ARCH),ppc64el)
  LDFLAGS = -k-znow
else
  LDFLAGS = -k-pie -k-znow
endif

lrc: *.pas po/*.po
	fpc $(CFLAGS) $(LDFLAGS) lrc.pas
	rstconv -i rstrings.rsj -o po/lrc.pot
	msgfmt -cv po/de.po -o po/de.mo
	msgfmt -cv po/es.po -o po/es.mo
	msgfmt -cv po/fr.po -o po/fr.mo
	msgfmt -cv po/it.po -o po/it.mo
	msgfmt -cv po/pt.po -o po/pt.mo
	msgfmt -cv po/sv.po -o po/sv.mo

clean:
	-rm *.o
	-rm *.ppu
	-rm *.res
	-rm *.rsj
	-rm po/*.mo
	-rm lrc
