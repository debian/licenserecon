// Reconcile DEP-5 debian/copyright to licensecheck
//
// Copyright : 2024 P Blackman
// License   : BSD-2-clause
//
// Test for false positive on licences;  Apache-2.0 v Apache-2
//
// Make generic dot-zero test
//  "For SPDX compatibility, trailing dot-zeroes are considered to be equal to plainer version
//  (e.g., "2.0.0" is considered equal to "2.0" and "2")."     [Dep5 specification]


unit dotzero;
{$mode delphi}

interface

function CheckDotZero (Dep5, Actual : String) : Boolean;


implementation uses StrUtils;


// Return true if Actual good match for d/copyright
function CheckDotZero (Dep5, Actual : String) : Boolean;
begin
    if (Dep5 = Actual) then
        result := true
    else
    if  (length (Actual) - length (Dep5) = 2) and (RightStr(Actual, 2) = '.0')
    or  (length (Actual) - length (Dep5) = 4) and (RightStr(Actual, 4) = '.0.0') then
        result := true
    else
        result := false;
end;

end.
