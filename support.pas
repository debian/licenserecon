// Reconcile DEP-5 debian/copyright to licensecheck
//
// Copyright : 2024 P Blackman
// License   : BSD-2-clause
//
// Self contained routines without global variable access


unit support;
{$mode delphi}

interface

const WhiteSpace :  set of Char = [' ',Chr(9)]; // Space & Tab

type
    tFileLic =
    record
        FName : AnsiString;
        Dep5,
        Actual : String;
    end;
    tFileLicArray = array of tFileLic;


procedure MangleName   (var Nam : AnsiString);
procedure UnMangleName (var Nam : AnsiString);
function OpenFile (const Name : AnsiString; out Myfile : text) : Boolean;


implementation uses StrUtils;

procedure MangleName (var Nam : AnsiString);
begin
    Nam := ReplaceStr (Nam, ' ', '\1'); // maybe spaces in filenames (segments then treated as separate files!)
    Nam := ReplaceStr (Nam, ',', '\2'); // commas are problems too
    Nam := ReplaceStr (Nam, '£', '\3');
    Nam := ReplaceStr (Nam, '*', '\4'); // wildcards
    Nam := ReplaceStr (Nam, '?', '\5'); // wildcard
end;

procedure UnMangleName (var Nam : AnsiString);
begin
    Nam := ReplaceStr (Nam, '\1', ' ');
    Nam := ReplaceStr (Nam, '\2', ',');
    Nam := ReplaceStr (Nam, '\3', '£');
    Nam := ReplaceStr (Nam, '\4', '*');
    Nam := ReplaceStr (Nam, '\5', '?');
end;


function OpenFile (const Name : AnsiString; out Myfile : text) : Boolean;
begin
    result := true;
    try
        AssignFile (Myfile, Name);
        Reset (Myfile);
    except
        result := false;
    end;
end;

end.
