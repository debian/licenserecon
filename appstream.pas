// Reconcile DEP-5 debian/copyright to licensecheck
//
// Copyright : 2024 P Blackman
// License   : BSD-2-clause
//
// Extract the license for an appstream metadata file
//  (workaround for https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1051840 )

unit appstream;
{$mode delphi}

interface

function GetMetadataLicense (Fname : AnsiString) : String;


implementation uses StrUtils, support;

function GetMetadataLicense (Fname : AnsiString) : String;

const maxlines : Integer = 100;
var lines,
    lpos : Integer;
    line : AnsiString;
    afile : Text;

begin
    result := '';
    lines := 0;
    if OpenFile (FName, afile) then
    begin
        while not EOF (afile) and (lines < maxlines) do
        begin
            Readln (afile, line);
            lpos := NPos ('<metadata_license>', line, 1);

            if lpos > 0 then
            begin
                Result := ExtractSubstr(line, lpos, ['<']);

                lines := maxlines // terminate loop
            end
            else
                lines := lines + 1;
        end;
        Close (afile);
     end;
end;

end.
