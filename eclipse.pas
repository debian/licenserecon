// Reconcile DEP-5 debian/copyright to licensecheck
//
// Copyright : 2024 P Blackman
// License   : BSD-2-clause
//
// Test for false detection of Eclipse license

unit eclipse;
{$mode delphi}

interface

function CheckEclipse (Fname : AnsiString; Dep5, Actual: String) : Boolean;


implementation uses StrUtils, Support;

// test for GPL+ style license
function CheckLicense (Fname : AnsiString) : Boolean;
const Max1 : Integer = 200; // Arbitrary, license usually at start of file

var Lines : Integer;
    E,EPL : Boolean;
    Line : AnsiString;
    Lfile : Text;

begin
    result  := false;
    E       := false;
    EPL     := false;
    Lines := 0;

    if OpenFile (FName, Lfile) then
    begin
        while not EOF (Lfile) and (Lines < Max1) do
        begin
            readln (Lfile, Line);

            E := E or (NPos ('Eclipse', Line, 1) > 0);

            If E then
                EPL := EPL or (NPos ('Eclipse Public License', Line, 1) > 0);

            if EPL then
                // Found 'Eclipse Public License'
                Lines  := Max1 // terminate loop
            else
                inc (Lines);
        end;

        if E and not EPL then
            // Found 'Eclipse' but not 'Eclipse Public License'
            result := true;

        Close (Lfile);
    end;
end;

// Return true if probable false detection of Eclipse license
function CheckEclipse (Fname : AnsiString; Dep5, Actual: String) : Boolean;
begin
    if    ContainsStr (Actual, 'EPL')
    and not ContainsStr (Dep5, 'EPL') then
        result := CheckLicense (FName)
    else
        result := false;
end;

end.
