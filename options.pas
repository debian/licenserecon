// Reconcile DEP-5 debian/copyright to licensecheck
//
// Copyright : 2024-2025 P Blackman
// License   : BSD-2-clause
//
// Process command line options

unit options;
{$mode delphi}

interface

var
    Option_Version : Boolean = False;
    Option_Help    : Boolean = False;
    Option_Struct  : Boolean = False;
    Option_Long    : Boolean = False;
    Option_Terse   : Boolean = False;
    Option_SPDX    : Boolean = False;


    ReadMeFile : Text;

    FallbackLang,
    Lang5   : Ansistring;
    Lang2   : string[2];

Procedure ShowVersions;
Procedure ShowHelp;
procedure CheckParam (P : String);
procedure GetOptions;


implementation uses Process, SysUtils, StrUtils, gettext, support, rstrings;

// Get Debian package version
function GetVersion (Package : String) : String;
var S1 : String;

begin
    if RunCommand ('/usr/bin/dpkg-query',
        ['-f', '''${Version}''', '-W', Package],
        S1, [poUsePipes, poWaitOnExit])
    then
        result := S1
    else
        result := '';
end;

Procedure ShowVersions;
begin
    Writeln (Lang2 + ': Versions: licenserecon ' + (GetVersion('licenserecon'))
            + '  licensecheck ' + (GetVersion('licensecheck')));
end;


Procedure ShowHelp;
var line   : String;
begin
    if not OpenFile ('/usr/share/doc/licenserecon/ReadMe.'+ Lang2, ReadMeFile) then
        OpenFile ('/usr/share/doc/licenserecon/ReadMe', ReadMeFile);

    While not EOF (ReadMeFile) Do
    begin
        Readln (ReadMeFile, Line);
        Writeln (Line);
    end;
end;


procedure CheckParam (P : String);
begin
    if SameText (P, '-v') or SameText (P, '--version') then
        Option_Version := true
    else if SameText (P, '-h') or SameText (P, '-?') or SameText (P, '--help') then
        Option_Help := True
    else if SameText (P, '-s') or SameText (P, '--struct') then
        Option_Struct := true
    else if SameText (P, '-t') or SameText (P, '--terse') then
        Option_Terse   := true
    else if SameText (P, '-l') or SameText (P, '--long') then
        Option_Long := true
    else if SameText (P, '-x') or SameText (P, '--spdx') then
        Option_SPDX := true
    else
        Writeln (rsIuo + ' ', P); // Ignoring unknown option
end;


procedure GetOptions;
var P : Integer;
begin
    for P := 1 to ParamCount do
        CheckParam (ParamStr(P));

    // This combination make no sense
    if Option_Terse then
        Option_Long := False;

    // Setup language id
    GetLanguageIDs(Lang5, FallbackLang);
    Lang2 := LeftStr(Lang5,2);
end;

end.
