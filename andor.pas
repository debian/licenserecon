// Reconcile DEP-5 debian/copyright to licensecheck
//
// Copyright : 2025 P Blackman
// License   : BSD-2-clause
//
// Allow 'or' in d/copyright to match with 'and/or' in dual licensing
// (and/or is very unlikely to be corect)

unit andor;
{$mode delphi}

interface

function CheckANDOR (Dep5, Actual: String) : Boolean;

implementation uses StrUtils;

// Return true if Actual has 'and/or' and Dep5 has 'or'
//  and that is only difference
function CheckANDOR (Dep5, Actual: String) : Boolean;
begin
    if  (length (Actual) - length (Dep5) = 4)
    and (Dep5 = ReplaceStr (Actual, 'and/or', 'or')) then
        result := true
    else
        result := false;
end;

end.
