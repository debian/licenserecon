Licenserecon: Verifique las licencias en el archivo Debian/copyright con licensecheck.

lrc analiza un archivo de derechos de autor DEP-5 válido y anota las licencias
de todos los archivos en el árbol de origen.
Luego se ejecuta Licensecheck y se comparan los resultados.
Diferencias entre licencias y versiones de licencia en debian/copyright
y se informa el resultado de la verificación de licencia.

Debe ejecutarse en el nivel superior de un árbol de fuentes de Debian limpio.
con un archivo de derechos de autor DEP-5 válido.
El árbol fuente debe estar limpio, de lo contrario, los resultados pueden verse
contaminados por informes falsos sobre la construcción. archivos generados.
Es recomendable ejecutar lintian primero para garantizar la sintaxis correcta.
de debian/copyright de autor.

Los resultados son sólo indicativos y no sustituyen a la comprobación manual.
Su objetivo es informar errores obvios. El diseño pretende minimizar las falsas
aspectos positivos tanto como sea práctico. Sin embargo, se producirán
falsos positivos si el La ortografía de la cadena corta de licencia no es
idéntica entre el archivo y debian/copyright de autor.
Esto es bastante probable con licencias complejas como
Construcciones 'y'/'o' y excepciones específicas.

Los falsos positivos pueden suprimirse creando un fichero debian/lrc.config
Enumere en el fichero, los nombres de ficheros y/o directorios a excluir.
La sintaxis del fichero se describe en /usr/share/lrc/lrc.config

Opciones de línea de comandos a utilizar en cada ejecución
(quizás --spdx etc con Salsa CI)
pueden incluirse en debian/lrc.config

Sólo se verifican los archivos con un encabezado de copyright.
Pueden producirse falsos negativos si licensecheck no puede determinar la
licencia de un archivo. Archivos denominados copyright,  copying, readme, etc.
no están marcados ya que a menudo especifican las licencias de otros archivos.
en lugar de los suyos propios.
Los archivos de Autotools generados automáticamente no están marcados,
ya que no es necesario que estén listados en debian/copyright,
y de lo contrario podría aparecer como una diferencia.

CÓDIGOS DE SALIDA
     0: No se encontraron diferencias
     1: Error al ejecutar (debian/copyright no válido)
     3: Se encontraron diferencias de licencia

SALIDA DE MUESTRA
     Salida de muestra que invoca lrc.

     ÉXITO:
        Parsing Source Tree  ....
        Reading copyright    ....
        Running licensecheck ....

        No differences found

     DIFERENCIAS:
        Parsing Source Tree  ....
        Reading copyright    ....
        Running licensecheck ....

        debian/copyright| licensecheck

        LGPL-2.1+       | GPL-2+       test/src/config/chan.c
        GPL-2+          | public-domain share/lua/int/dummy.lua
        GPL-2+          | LGPL-2.1+    modules/access/sr_common.h

OPCIONES
Las opciones no distinguen entre mayúsculas y minúsculas.
Las opciones no válidas se ignoran.

-? -h o --help
Escribe este archivo Léame en la salida estándar.

-l o --long
Se genera una salida para cada archivo donde licencecheck detecta la licencia,
no sólo aquellos con discrepancias en las licencias.

-s o --struct
Salida estructurada.
Genera la licencia debian/copyright, la licencia de licensecheck y
el nombre del archivo. en tres líneas separadas, seguidas de una línea en blanco.
Se suprimen los encabezados y líneas de resumen.
La información de la versión no se muestra a menos que se especifique mediante
la opción -v.

-t o --terse
Donde se generarían bloques de archivos con diferencias de licencia idénticas,
sólo se muestra el primer archivo. Anula -l (si se especifica)

-v o --versión
Escribe los números de versión de licenserecon y licenscheck
en la salida estándar y luego sale.

-x or --spdx
Espera nombres cortos de licencia estilo SPDX.
(Por defecto es debian DEP 5 nombres cortos)

PIPELINES SALSA CI
lrc puede ser incluido en Salsa CI Pipelines usando
 debian/salsa-ci.yml@debian/licenserecon
como archivo de configuración CI/CD.
