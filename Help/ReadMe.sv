licenserecon: Kontrollera licenser i debian/copyright-filen mot licensecheck.

lrc analyserar en giltig DEP-5-upphovsrättsfil och noterar licenserna för alla
filer i källträdet. Licenscheck körs sedan och resultaten jämförs.
Skillnader mellan licenser och licensversioner i debian/copyright
och utdata från licenskontroll rapporteras.

Det bör köras på den översta nivån av ett rensat Debian-källträd,
med en giltig DEP-5-upphovsrättsfil. Källträdet ska vara rent,
annars kan resultaten vara förorenade av falska rapporter om byggnaden
genererade filer. Det är tillrådligt att köra lintian först för att säkerställa
korrekt syntax av debian/copyright.

Resultaten är endast vägledande och inte en ersättning för manuell kontroll.
Det är tänkt att rapportera uppenbara fel. Designen avser att minimera falska
positiva så mycket som är praktiskt. Däremot kommer falska positiva resultat att
inträffa om stavningen av licensens kortsträng är inte identisk mellan filen och
debian/copyright. Detta är ganska troligt med komplex licensiering som t.ex
'och'/'eller'-konstruktioner och specifika undantag.

Falska positiva resultat kan undertryckas genom att skapa
en fil debian/lrc.config
I filen listas de filnamn och/eller kataloger som ska uteslutas.
Syntaxen för filen beskrivs i /usr/share/lrc/lrc.config

Kommandoradsalternativ som ska användas vid varje körning
(kanske --spdx etc med Salsa CI)
kan inkluderas i debian/lrc.config

Endast filer med upphovsrättsrubriker kontrolleras. Falskt negativ kan uppstå om
licensecheck kan inte fastställa en fils licens. Filer med namnet copyright,
kopiering, readme etc. kontrolleras inte eftersom de ofta anger licenserna för
andra filer snarare än sina egna. Autogenererade Autoverktygs filer är inte
kontrollerade, eftersom de inte behöver listas i debian/copyright,
och annars skulle kunna framstå som en skillnad.

AVSLUTA KODER
     0: Inga skillnader hittades
     1: Det gick inte att köra (ingen giltig debian/copyright)
     3: Licensskillnader hittades

EXEMPEL UTGÅNG
     Exempelutgång som anropar lrc.

     FRAMGÅNG:
        Parsing Source Tree  ....
        Reading copyright    ....
        Running licensecheck ....

        No differences found

     SKILLNADER:
        Parsing Source Tree  ....
        Reading copyright    ....
        Running licensecheck ....

        debian/copyright| licensecheck

        LGPL-2.1+       | GPL-2+       test/src/config/chan.c
        GPL-2+          | public-domain share/lua/int/dummy.lua
        GPL-2+          | LGPL-2.1+    modules/access/sr_common.h

ALTERNATIV
Alternativen är skiftlägesokänsliga. Ogiltiga alternativ ignoreras.

-? -h eller --help
Skriver denna readme-fil till stdout.

-l eller --long
Utdata genereras för varje fil där licensecheck upptäcker licensen,
inte bara de med licensavvikelser.

-s eller --struct
Strukturerad utgång.
Matar ut debian/copyright-licensen, licensecheck-licensen och filnamnet
på tre separata rader, följt av en tom rad.
Rubriker och sammanfattningsrader är undertryckta. Versionsinformation matas
inte ut om det inte anges via -v-alternativet.

-t eller --terse
Där filblock med identiska licensskillnader skulle matas ut,
endast den första filen visas. Åsidosätter -l (om angivet)

-v eller --version
Skriver versionsnumren för licenserecon och licenscheck
till stdout och avslutar sedan

-x or --spdx
Förväntar sig SPDX-liknande kortnamn för licenser.
(Standard är debian DEP 5 kortnamn)

SALSA CI PIPELINE
lrc kan inkluderas i Salsa CI Pipelines genom att använda
 debian/salsa-ci.yml@debian/licenserecon
som CI/CD-konfigurationsfil.
