licenserecon: controlla le licenze nel file Debian/copyright rispetto a licensecheck.

lrc analizza un file di copyright DEP-5 valido e annota le licenze di tutti i
file nell'albero dei sorgenti. Viene quindi eseguito il controllo della licenza e
i risultati vengono confrontati.
Differenze tra licenze e versioni di licenza in debian/copyright
e vengono riportati l'output di licensecheck.

Dovrebbe essere eseguito al livello più alto di un albero dei sorgenti
Debian pulito, con un file di copyright DEP-5 valido.
L'albero dei sorgenti dovrebbe essere pulito, in caso contrario i risultati
potrebbero essere contaminati da rapporti falsi sulla build file generati.
È consigliabile eseguire prima lintian per garantire la corretta sintassi
di debian/copyright.

I risultati sono solo indicativi e non sostituiscono il controllo manuale.
Lo scopo è segnalare errori evidenti. Il design intende ridurre al minimo i
falsi positivi tanto quanto pratici. Tuttavia, si verificheranno falsi positivi
se l'ortografia della stringa breve della licenza non è identica tra il file e
debian/copyright. Ciò è molto probabile con licenze complesse come
costrutti 'e'/'o' ed eccezioni specifiche.

I falsi positivi possono essere eliminati creando un file debian/lrc.config
Elencare nel file i nomi dei file e/o delle directory da escludere.
La sintassi del file è descritta in /usr/share/lrc/lrc.config

Opzioni della riga di comando da usare a ogni esecuzione
(forse --spdx ecc. con Salsa CI) possono essere incluse in debian/lrc.config

Vengono controllati solo i file con un'intestazione protetta da copyright.
Possono verificarsi falsi negativi se licensecheck non è in grado di determinare
la licenza di un file. File denominati copyright, copia, readme ecc. non vengono
controllati poiché spesso specificano le licenze di altri file piuttosto che il
proprio. I file degli Autotools generati automaticamente non vengono controllati,
poiché non è necessario che siano elencati in debian/copyright,
e potrebbe altrimenti apparire come una differenza.

CODICI DI USCITA
     0: nessuna differenza trovata
     1: Impossibile eseguire (nessuna Debian/copyright valida)
     3: Rilevate differenze di licenza

USCITA CAMPIONE
     Esempio di output che invoca lrc.

    SUCCESSO:
        Parsing Source Tree  ....
        Reading copyright    ....
        Running licensecheck ....

        No differences found

    DIFFERENZE:
        Parsing Source Tree  ....
        Reading copyright    ....
        Running licensecheck ....

        debian/copyright| licensecheck

        LGPL-2.1+       | GPL-2+       test/src/config/chan.c
        GPL-2+          | public-domain share/lua/int/dummy.lua
        GPL-2+          | LGPL-2.1+    modules/access/sr_common.h

OPZIONI
Le opzioni non fanno distinzione tra maiuscole e minuscole.
Le opzioni non valide vengono ignorate.

-? -h o --help
Scrive questo file readme su stdout.

-l o --long
L'output viene generato per ogni file in cui licensecheck rileva la licenza,
non solo quelli con discrepanze di licenza.

-s o --struct
Uscita strutturata.
Restituisce la licenza debian/copyright, la licenza licensecheck e il nome del
file su tre righe separate, seguite da una riga vuota.
Le intestazioni e le righe di riepilogo vengono soppresse.
Le informazioni sulla versione non vengono visualizzate
a meno che non sia specificato tramite l'opzione -v.

-t o --terse
Laddove venissero emessi blocchi di file con identiche differenze di licenza,
viene mostrato solo il primo file. Sostituisce -l (se specificato)

-v o --version
Scrive i numeri di versione di Licenserecon e LicensCheck su stdout, quindi esce

-x or --spdx
Si aspetta nomi brevi di licenze in stile SPDX.
(L'impostazione predefinita è quella dei nomi brevi di debian DEP 5).

SALSA CI PIPELINE
lrc può essere incluso nelle pipeline di Salsa CI usando
 debian/salsa-ci.yml@debian/licenserecon
come file di configurazione CI/CD.
